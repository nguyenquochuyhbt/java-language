package javalanguagebasic.variables;

public class StaticVariables {

	public static String a;// global variable - all method can use it
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		a = "Huy";
		System.out.println(a);
		function();
		System.out.println(a);
	}
	
	private static void function() {// function must use static if use static variable
		a = "Nguyen";
	}

}
