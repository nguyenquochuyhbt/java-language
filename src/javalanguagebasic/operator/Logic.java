package javalanguagebasic.operator;

public class Logic {

	static int a = 0;
	static int b = 2;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// && || ^ !
		
		boolean rs = (a==0 && b<2) ? true : false;
		System.out.println(rs);
		
		rs = (a==1 || b==2) ? true : false;
		System.out.println(rs);
		
		rs = (a==0 ^ b==2) ? true : false;// if only value return true => true, else all return false
		System.out.println(rs);
		
		rs = (a==0 ^ b<2) ? true : false;
		System.out.println(rs);
		
		boolean check = true;
		check = false;
		rs = (!check) ? true : false;// ! only use boolean variable
		System.out.println(rs);
	}

}
